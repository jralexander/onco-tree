from flask import render_template
from flask import Flask, make_response, abort, url_for, redirect, request, jsonify
from time import sleep
import json
import requests

app = Flask(__name__)

results = []

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/file.txt')
def return_onco_file():
    global results
    results = []
    s = get_tree()
    print(s)

    return s


def add_node(node):
    onco = {
        'code': node['code'] if 'code' in node.keys() else '',
        'color': node['color'] if 'color' in node.keys() else '',
        'name': node['name'] if 'name' in node.keys() else '',
        'mainType': node['mainType'] if 'mainType' in node.keys() else '',
        'tissue': node['tissue'] if 'tissue' in node.keys() else '',
        'parent': node['parent'] if 'parent' in node.keys() else '',
        'level': node['level'] if 'level' in node.keys() else '',
        'UMLS': '',
        'NCI': ''
    }
    if 'externalReferences' in node.keys():
        refs = ['UMLS', 'NCI']
        index = 0
        additional_keys = True
        while additional_keys:
            additional_keys = False
            for key in refs:
                if key in node['externalReferences'].keys():
                    if node['externalReferences'][key]:
                        onco[key] = node['externalReferences'][key][index]
                        if (index + 1) < len(node['externalReferences'][key]):
                            print(f'Addl Keys: Code: {node["code"]} - key: {key} - index: {index}')
                            additional_keys = True
            index += 1
            if additional_keys:
                tmp = dict(onco)
                results.append(tmp)

    results.append(onco)

    if 'children' not in node.keys():
        return

    for key in node['children'].keys():
        add_node(node['children'][key])

def get_tree():
    url = 'http://oncotree.mskcc.org/api/tumorTypes/tree'
    tree = requests.get(url).json()

    add_node(tree['TISSUE'])
    pipe_file = 'code|color|name|mainType|tissue|parent|level|UMLS|NCI\n'
    for r in results:
        pipe_file += f"""{r["code"]}|{r['color']}|{r['name']}|{r['mainType']}|{r['tissue']}|{r['parent']}|{r['level']}|{r["UMLS"]}|{r["NCI"]}\n"""

    return pipe_file

