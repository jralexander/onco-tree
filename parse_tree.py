import json
import requests

results = []

def add_node(node):
    onco = {
        'code': node['code'] if 'code' in node.keys() else '',
        'color': node['color'] if 'color' in node.keys() else '',
        'name': node['name'] if 'name' in node.keys() else '',
        'mainType': node['mainType'] if 'mainType' in node.keys() else '',
        'tissue': node['tissue'] if 'tissue' in node.keys() else '',
        'parent': node['parent'] if 'parent' in node.keys() else '',
        'level': node['level'] if 'level' in node.keys() else '',
        'UMLS': '',
        'NCI': ''
    }
    if 'externalReferences' in node.keys():
        refs = ['UMLS', 'NCI']
        index = 0
        additional_keys = True
        while additional_keys:
            additional_keys = False
            for key in refs:
                if key in node['externalReferences'].keys():
                    if node['externalReferences'][key]:
                        onco[key] = node['externalReferences'][key][index]
                        if (index + 1) < len(node['externalReferences'][key]):
                            print(f'Addl Keys: Code: {node["code"]} - key: {key} - index: {index}')
                            additional_keys = True
            index += 1
            if additional_keys:
                tmp = dict(onco)
                results.append(tmp)

    results.append(onco)

    if 'children' not in node.keys():
        return

    for key in node['children'].keys():
        add_node(node['children'][key])

def main():
    url = 'http://oncotree.mskcc.org/api/tumorTypes/tree'
    tree = requests.get(url).json()
    # with open('tree.json') as f:
    #     tree = json.load(f)

    add_node(tree['TISSUE'])

    with open('output.txt', 'w') as o:
        o.write('code|color|name|mainType|tissue|parent|level|UMLS|NCI\n')
        for r in results:
            o.write(f"""{r["code"]}|{r['color']}|{r['name']}|{r['mainType']}|{r['tissue']}|{r['parent']}|{r['level']}|{r["UMLS"]}|{r["NCI"]}\n""")

if __name__ == '__main__':
    main()
